package com.example.sihyuk.simplecalculator;

public class Calculate {
    public double a, b, r;
    public char op;

    public Calculate(){
        op = '0';
    }

    public double result(){
        switch(op){
            case '+':
                r = a+b;
                break;
            case '-':
                r = a-b;
                break;
            case '*':
                r = a*b;
                break;
            case '/':
                r = a/b;
                break;
            default :
                r = a;
        }
        System.out.println(r);
        return Double.parseDouble(""+r);
    }
}
