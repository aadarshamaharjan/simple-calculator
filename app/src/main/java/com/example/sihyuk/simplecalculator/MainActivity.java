package com.example.sihyuk.simplecalculator;

import android.net.wifi.p2p.WifiP2pManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.widget.Toast.makeText;

public class MainActivity extends AppCompatActivity {

    EditText edt;
    TextView vt;
    Calculate cal;
    char op;
    int f;
    String a, b;
    int x, y, i;
    char chr[] = {'+','-','*','/'};
    Boolean eq = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        edt = (EditText) findViewById(R.id.edt1);
        vt=(TextView) findViewById(R.id.viewtext);
        cal=new Calculate();
        a = "";
        b = "";
        f=1;
        op = '0';
    }
    public void numCheck(String i){
        if(eq)
            a = "";
        if(cal.op == '0'){
            a += i;
            edt.setText(a+" ");
        }
        else{
            b += i;
            if(b.contains("-"))
                edt.setText(cal.op+" ("+b+") ");
            else
                edt.setText(cal.op+" "+b+" ");
        }
        eq = false;
    }
    public void opCheck(int i){
        if(b!=""){

            cal.a = Double.parseDouble(a);
            cal.b = Double.parseDouble(b);
            display(cal.result());
            a=String.valueOf(edt.getText());
            b = "";
            cal.op = '0';
        }
//                System.out.println(a);
        if(f==0)
        cal.a = Double.parseDouble(a);
//                cal.b=Double.parseDouble(b);
        cal.op = chr[i];
        vt.setText(a+"     ");
        edt.setText(cal.op+" ");
        eq = false;
        f=1;
    }
    public void equals(){
        eq = true;
        if (a != "")
            cal.a = Double.parseDouble(a);
        else
            cal.a = 0;
        if (b != "")
            cal.b = Double.parseDouble(b);
        else
            cal.b = 0;
        if (cal.op != '0' && cal.b != 0) {
            if (cal.b >= 0)
                vt.setText(a + " " + cal.op + " " + b + "     ");
            else
                vt.setText(a + " " + cal.op + " (" + b + ")     ");
        } else
            vt.setText(a + "     ");
        display(cal.result());
        String edtval=String.valueOf(edt.getText());
        int val=edtval.length()-1;
        String ed=(edtval).substring(0,val);
        a = ed;
        a = a.replace(",", "");
        b = "";
        cal.op = '0';
    }
    public void delete(){
        if (!eq) {
            String str = "0";
            if (b.length() > 1) {
                b = b.substring(0, b.length() - 1);
                edt.setText(cal.op + " " + b + " ");
            } else if (b.length() == 1 && b != "0") {
                b = "";
                edt.setText(cal.op + " ");
            } else if (cal.op != '0') {
                cal.op = '0';
                vt.setText("");
                edt.setText(a + " ");
            } else if (a.length() > 1) {
                a = a.substring(0, a.length() - 1);
                edt.setText(a + " ");
            } else if (a.length() == 1 && a != "0") {
                a = "";
                edt.setText("0 ");
            }
        }
    }
    public void clear(){
        a = "";
        b = "";
        cal.op = '0';
        vt.setText("");
        edt.setText("0 ");
    }
    public void decimal(){
        if (eq) {
            a = "";
            eq = false;
        }
        if (cal.op == '0') {
            if (!a.contains("."))
                a += ".";
            edt.setText(a + " ");
            System.out.println(a);
        } else {
            if (!b.contains("."))
                b += ".";
            edt.setText(cal.op + " " + b + " ");
            System.out.println(b);
        }
    }
    public void negative(){
        if (cal.op != '0') {
            if (b.contains("-")) {
                b = b.substring(1, b.length());
                edt.setText(cal.op + " " + b + " ");
            } else {
                b = '-' + b;
                edt.setText(cal.op + " (" + b + ") ");
            }
        } else if (a != "") {
            if (a.contains("-")) {
                a = a.substring(1, a.length());
            } else {
                a = "-" + a;
            }
            edt.setText(a + " ");
        }
    }
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.button0:
                    numCheck("0");
                    break;
                case R.id.button00:
                    numCheck("00");
                    break;
                case R.id.button1:
                    numCheck("1");
                    break;
                case R.id.button2:
                    numCheck("2");
                    break;
                case R.id.button3:
                    numCheck("3");
                    break;
                case R.id.button4:
                    numCheck("4");
                    break;
                case R.id.button5:
                    numCheck("5");
                    break;
                case R.id.button6:
                    numCheck("6");
                    break;
                case R.id.button7:
                    numCheck("7");
                    break;
                case R.id.button8:
                    numCheck("8");
                    break;
                case R.id.button9:
                    numCheck("9");
                    break;
                case R.id.buttonadd:
                    opCheck(0);
                    break;
                case R.id.buttonsub:
                    opCheck(1);
                    break;
                case R.id.buttonmul:
                    opCheck(2);
                    break;
                case R.id.buttondiv:
                    opCheck(3);
                    break;
                case R.id.buttoneql:
                    equals();
                    break;
                case R.id.buttonDel:
                    delete();
                    break;
                case R.id.buttonC:
                    clear();
                    break;
                case R.id.button10:
                    decimal();
                    break;
                case R.id.buttonneg:
                    negative();
                    break;
                case R.id.buttonpow:
                    System.exit(0);
                default:

            }


        }
    public void display(double q){
        long s = (long) q;
        if(s==q)
            edt.setText((s)+" ");
        else
            edt.setText((q)+" ");
    }
    public void display(String z){
        edt.setText(z+" ");
    }


}
